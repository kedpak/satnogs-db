{% extends 'emails/base.txt' %}

{% block main %}
New suggestion for Satellite {{ data.satname }} was submitted by user {{ data.contributor }}!

{{ data.saturl }}

Navigate to {{ data.sitedomain }}/admin/base/suggestion/ to review and approve it.
{% endblock %}
